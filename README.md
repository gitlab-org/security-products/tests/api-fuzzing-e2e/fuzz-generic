# Generic API Fuzzing E2E Test project

Use to include new E2E tests in API Fuzzing project.

1. Configure DAST E2E Test project
   1. If needed add resources into directory `assets/<your-project>`
1. Add a new job in the API Fuzzing project 

<!-- 
har-config-root-location
har-remote-target
 -->